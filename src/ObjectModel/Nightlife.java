package ObjectModel;

import com.Function.MainDriver;
import org.openqa.selenium.WebElement;

public class Nightlife extends MainDriver { //klasický object model na noční zivot.
//    public static WebElement MujKlub(){
//        return driver.findElement(By.xpath("//*[@align=\"right\"]/div/button"));
//    }
////    public static WebElement Kolonka(){  //xpath na kolonku: /html/body/div[2]/div[3]/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td/div[2]/div/div[3]/table[2]/tbody/tr/td[4]/input
////        return driver.findElement(By.id("nightclub-drug-quantity-2"));
////    } od nové verze není k dispozici.
//    public static WebElement kup(){  //xpath na tlačítko Kup: /html/body/div[2]/div[3]/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td/div[2]/div/div[3]/table[2]/tbody/tr/td[4]/button
//        return driver.findElement(By.id("nightclub-drug-buy-button-2"));
//    }
//    public static WebElement vypadni(){  //xpath na tlačítko vypadni: /html/body/div[2]/div[3]/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td/div[2]/div/div[3]/div[1]/button
//        return driver.findElement(By.xpath("//*[@class=\"btn btn-inverse btn-large pull-right\"]"));
//        //driver.findElement(By.id("#exit-button-ZmWXm55onJpxZGaYxmSax2aXmMVvn5qcn2WcxWhrl5VhZZXFyQ")) zkoušené ID tlačítko, avšak nefunguje.
//        //*[@id="exit-button"]
//        //html/body/div[2]/div[4]/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td/div[2]/div/div[3]/div[1]/button
//    }


//    // novy kod:
//
//    public  WebElement GetVstupXpath() {
//        return driver.findElement(By.id("menu-nightlife"));
//    }
//    private WebElement _vstupWebElement;
//    public void SetVstupWebElement(WebElement webElement) {
//        _vstupWebElement = webElement;
//    }
//    public WebElement GetVstupWebElement() {
//        return _vstupWebElement;
//    }

// Xpath na první cizí klub //*[@class="well well-small"]//*[@class="btn btn-inverse btn-small pull-right"]


    public  String GetMenuNightlifeXpath() {return "//*[@id=\"menu-nightlife\"]"; }
    private WebElement _menuNightlifeWebElement;
    public void SetMenuNightlifeElement(WebElement webElement) {_menuNightlifeWebElement = webElement; }
    public WebElement GetMenuNightlifeWebElement() {return _menuNightlifeWebElement; }

    public  String GetMyClubXpath() {return "//*[@align=\"right\"]/div/button"; }
    private WebElement _myClubWebElement;
    public void SetMyClubWebElement(WebElement webElement) {_myClubWebElement = webElement; }
    public WebElement GetMyClubWebElement() {return _myClubWebElement; }

    public  String GetBuyXpath() {return "//*[contains(@id,'drug-buy-button')]";    }
    private WebElement _buyWebElement;
    public void SetBuyWebElement(WebElement webElement) {_buyWebElement = webElement;}
    public WebElement GetBuyWebElement() {return _buyWebElement;}

    public  String GetExitXpath() {return "//*[contains(@id,'exit-button')]";}
    private WebElement _exitWebElement;
    public void SetExitWebElement(WebElement webElement) {_exitWebElement = webElement;}
    public WebElement GetExitWebElement() {return _exitWebElement;}

    public  String GetAssaultSelectorXpath() {
        return "//*[@class=\"btn btn-inverse dropdown-toggle\"]";
    }
    private WebElement _assaultSelectorWebElement;
    public void SetAssaultSelectorElement(WebElement webElement) {
        _assaultSelectorWebElement = webElement;
    }
    public WebElement GetAssaultSelectorWebElement() {
        return _assaultSelectorWebElement;
    }

    public  String GetSingleAssaultXpath() {
        return "//*[contains(text(),'Attack') or contains(text(),'Srovnej')]";
    }
    private WebElement _singleAssaultWebElement;
    public void SetSingleAssaultElement(WebElement webElement) {
        _singleAssaultWebElement = webElement;
    }
    public WebElement GetSingleAssaultWebElement() {
        return _singleAssaultWebElement;
    }

    public  String GetRandomClubXpath() {return "//*[@class=\"well well-small\"]//*[@class=\"btn btn-inverse btn btn-inverse btn-small pull-right\"]";}  //*[@class="well well-small"]//*[@class="btn btn-inverse btn-small pull-right"] 2: //*[@class=\"well well-small\"][text()='Párty Rave' or 'Rave Party']//parent::div//parent::div//*[@class=\"btn btn-inverse btn-small pull-right\"]
    private WebElement _randomClubWebElement;
    public void SetRandomClubElement(WebElement webElement) {
        _randomClubWebElement = webElement;
    }
    public WebElement GetRandomClubWebElement() {
        return _randomClubWebElement;
    }

    public  String GetAssaultTargetInSelectXpath() {return "//*[contains(@id,'nightclub-select-assault-type-single')]"; }
    private WebElement _assaultTargetInSelectWebElement;
    public void SetAssaultTargetInSelectElement(WebElement webElement){_assaultTargetInSelectWebElement = webElement; }
    public WebElement GetAssaultTargetInSelectWebElement() {return _assaultTargetInSelectWebElement; }

    public  String GetTargetRespectXpath() {return "//*[@id=\"content_middle\"]//*[contains(text(), 'Respect') or contains(text(), 'Respekt')]";}
    private WebElement _targetRespectRobWebElement;
    public void SetTargetRespectWebElement(WebElement webElement) {_targetRespectRobWebElement = webElement;}
    public WebElement GetTargetRespectWebElement() {return _targetRespectRobWebElement;}

    public  String GetH1textXpath() {return "//*[@class=\"content_style main-content \"]/div[3]/h1"; }
    private WebElement _h1TextWebElement;
    public void SetH1TextElement(WebElement webElement) {_h1TextWebElement = webElement; }
    public WebElement GetH1TextWebElement() {return _h1TextWebElement; }

    public  String GetTargetClassXpath() {return "//*[@id=\"content_middle\"]//*[contains(text(), 'Hitman') or contains(text(), 'Vazoun') or contains(text(), 'Pimp') or contains(text(), 'Pasák') or contains(text(), 'Robber') or contains(text(), 'Zloděj') or contains(text(), 'Dealer') or contains(text(), 'Businessman') or contains(text(), 'Obchodník') or contains(text(), 'Broker') or contains(text(), 'Makléř') or contains(text(), 'Vazoun')]"; }
    private WebElement _targetClassWebElement;
    public void SetTargetClassElement(WebElement webElement) {_targetClassWebElement = webElement; }
    public WebElement GetTargetClassWebElement() {return _targetClassWebElement; }
    //*[@id=\"content_middle\"]//*[contains(text(), 'Hitman') or contains(text(), 'Vazoun') or contains(text(), 'Pimp') or contains(text(), 'Pasák') or contains(text(), 'Robber') or contains(text(), 'Zloděj') or contains(text(), 'Dealer') or contains(text(), 'Businessman') or contains(text(), 'Obchodník') or contains(text(), 'Broker') or contains(text(), 'Makléř') or contains(text(), 'Vazoun')]
}
