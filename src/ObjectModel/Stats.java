package ObjectModel;
import com.Function.MainDriver;
import org.openqa.selenium.WebElement;

//     //*[@class="text-center"]/div[contains(text(), 'Výdrž'. 'Stamina')]
public class Stats extends MainDriver {           //  /html/body/div[2]/div[3]/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td/div[3]/div/div[4]/div[1]
    public  String GetEnergyXpath() {return "//*[@class=\"text-center\"]/div[contains(text(), 'Stamina') or contains(text(), 'Výdrž')]";}
    private WebElement _energyWebElement;
    public void SetEnergyElement(WebElement webElement) {
        _energyWebElement = webElement;
    }
    public WebElement GetEnergyWebElement() {
        return _energyWebElement;
    }

    public  String GetAddictionXpath() {
        return "//*[@class=\"text-center\"]/div[contains(text(), 'Addiction') or contains(text(), 'Závislost')]";}
    private WebElement _addictionWebElement;
    public void SetAddictionElement(WebElement webElement) {
        _addictionWebElement = webElement;
    }
    public WebElement GetAddictionWebElement() {
        return _addictionWebElement;
    }

    public  String GetSingleRobberyPowerXpath() {return "//*[@id=\"content_right\"]/div/div[8]/div[1]";}
    private WebElement _singleRobberyPowerWebElement;
    public void SetSingleRobberyPowerElement(WebElement webElement) {_singleRobberyPowerWebElement = webElement;}
    public WebElement GetSingleRobberyPowerWebElement() {return _singleRobberyPowerWebElement;}
    //*[@id="user-profile-info"]/div[8]/span můj respekt
    public  String GetMyRespectRobXpath() {return "//*[@id=\"user-profile-info\"]/div[contains(text(), 'Respect') or contains(text(), 'Respekt')]/span";}
    private WebElement _myRespectRobWebElement;
    public void SetMyRespectWebElement(WebElement webElement) {_myRespectRobWebElement = webElement;}
    public WebElement GetMyRespectWebElement() {return _myRespectRobWebElement;}

    public  String GetStrengthXpath() {return "//*[@class=\"user_profile_stats pull-right text-center\"]/br[1]//following-sibling::span"; }
    private WebElement _strengthWebElement;
    public void SetStrengthElement(WebElement webElement) {_strengthWebElement = webElement; }
    public WebElement GetStrengthWebElement() {return _strengthWebElement; }

    public  String GetToleranceXpath() {return "//*[@class=\"user_profile_stats pull-right text-center\"]/br[3]//following-sibling::span"; }
    private WebElement _toleranceWebElement;
    public void SetToleranceElement(WebElement webElement) {_toleranceWebElement = webElement; }
    public WebElement GetToleranceWebElement() {return _toleranceWebElement; }

    public  String GetIntelligenceXpath() {return "//*[@class=\"user_profile_stats pull-left text-center\"]/br[1]//following-sibling::span"; }
    private WebElement _intelligenceWebElement;
    public void SetIntelligenceElement(WebElement webElement) {_intelligenceWebElement = webElement; }
    public WebElement GetIntelligenceWebElement() {return _intelligenceWebElement; }

    public  String GetCharismaXpath() {return "//*[@class=\"user_profile_stats pull-left text-center\"]/br[3]//following-sibling::span"; }
    private WebElement _charismaWebElement;
    public void SetCharismaElement(WebElement webElement) {_charismaWebElement = webElement; }
    public WebElement GetCharismaWebElement() {return _charismaWebElement; }



    //*[@class="content_style main-content "]/div/div/ul/li/div/div[contains(text(), 'Respect') or contains(text(), 'Respekt')]/span
//*[@class="content_style main-content "]//*[@class="btn btn-inverse btn-small pull-right" and @class="well well-small"]//*[contains(text(), 'Párty Rave') or contains(text(), 'Rave Party')]


}
