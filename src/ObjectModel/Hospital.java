package ObjectModel;

import com.Function.MainDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Hospital extends MainDriver {
    public static WebElement vstup (){
        return driver.findElement(By.id("//*[@id=\"menu-sprite-hospital\"]"));
    }
    public static WebElement detoxikace (){
        return driver.findElement(By.xpath("//*[@id=\"content_middle\"]/div/div[3]/table[2]/tr/td[2]/button[1]"));
    }

    public  String GetMenuHospitalXpath() {
        return "//*[@id=\"menu-sprite-hospital\"]";
    }
    private WebElement _menuHospitalWebElement;
    public void SetMenuHospitalElement(WebElement webElement) {
        _menuHospitalWebElement = webElement;
    }
    public WebElement GetMenuHospitalWebElement() {return _menuHospitalWebElement;}

    public  String GetDetoxifyXpath() {
        return "//*[@class=\"btn btn-small btn-inverse pull-left\"]";
    }
    private WebElement _DetoxifyWebElement;
    public void SetDetoxifyWebElement(WebElement webElement) {
        _DetoxifyWebElement = webElement;
    }
    public WebElement GetDetoxifyWebElement() {
        return _DetoxifyWebElement;
    }

}
