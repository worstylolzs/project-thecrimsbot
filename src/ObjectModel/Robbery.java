package ObjectModel;
import com.Function.MainDriver;
import org.openqa.selenium.WebElement;


public class Robbery extends MainDriver {
    private MainDriver mainDriver = new MainDriver();
//    public static WebElement menuzlodejiny () { return driver.findElement(By.xpath("//*[@id=\"menu-robbery\"]"));}
//    public static WebElement vyber (){
//        return driver.findElement(By.id("//*[@id=\"singlerobbery-select-robbery\"]"));
//    }
//    public static WebElement tlacitko (){
//        return driver.findElement(By.xpath("//*[@id=\"singlerobbery-rob\"]"));
//    }

    public  String GetMenuRobberyXpath() {
        return "//*[@id=\"menu-robbery\"]";
    }
    private WebElement _menuRobberyWebElement;
    public void SetMenuRobberyElement(WebElement webElement) {
        _menuRobberyWebElement = webElement;
    }
    public WebElement GetMenuRobberyWebElement() {
        return _menuRobberyWebElement;
    }

    public  String GetRobberySelectXpath() {
        return "//*[@id=\"singlerobbery-select-robbery\"]";
    }
    private WebElement _robberySelectWebElement;
    public void SetRobberySelectWebElement(WebElement webElement) {
        _robberySelectWebElement = webElement;
    }
    public WebElement GetRobberySelectWebElement() {return _robberySelectWebElement; }

    public  String GetRobberyRobXpath() {
        return "//*[@id=\"singlerobbery-rob\"]";
    }
    private WebElement _robberyRobWebElement;
    public void SetRobberyRobWebElement(WebElement webElement) {
        _robberyRobWebElement = webElement;
    }
    public WebElement GetRobberyRobWebElement() {
        return _robberyRobWebElement;
    }

    public  String GetGangRobberyRobXpath() {
        return "//*[@id=\"gangrobbery-accept\"]";
    }
    private WebElement _gangRobberyRobWebElement;
    public void SetGangRobberyRobWebElement(WebElement webElement) {
        _gangRobberyRobWebElement = webElement;
    }
    public WebElement GetGangRobberyRobWebElement() {
        return _gangRobberyRobWebElement;
    }

    public  String GetGangRobberyExecuteXpath() {return "//*[@id=\"gangrobbery-execute\"]"; }
    private WebElement _gangRobberyExecuteWebElement;
    public void SetGangRobberyExecuteElement(WebElement webElement) {_gangRobberyExecuteWebElement = webElement; }
    public WebElement GetGangRobberyExecuteWebElement() {return _gangRobberyExecuteWebElement; }

}