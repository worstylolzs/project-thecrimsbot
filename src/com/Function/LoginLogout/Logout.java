package com.Function.LoginLogout;

import com.Function.MainDriver;
import org.openqa.selenium.By;

public class Logout extends MainDriver {
    public static void odhlasuji(){
        System.out.println("Zvolili jste odhlášení klienta.");
        driver.findElement(By.xpath("//*[@class='navbar navbar-inverse navbar-fixed-top']//*[@href='/logout']")).click();

        System.out.println("Klient byl úspěšně odhlášen");
    }

    public static void konecprogramu(){
    driver.quit();
    }
    }