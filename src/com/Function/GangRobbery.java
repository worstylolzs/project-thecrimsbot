package com.Function;

import ObjectModel.Robbery;
import com.Function.Filters.StatsValue;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.TimeoutException;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;

public class GangRobbery extends MainDriver {
    public static void gang_robbery_click() throws InterruptedException, InvocationTargetException, FileNotFoundException, IllegalAccessException, NoSuchMethodException { // manualní kradení, stará sekce, která už se vlastně nepoužívá, není potřeba...
        StatsValue statsvalue = new StatsValue();
        Robbery robbery = new Robbery();
        int Energyo = Integer.parseInt(statsvalue.EnergyValue());
        ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, robbery.GetMenuRobberyXpath(),10).click();
        while (Energyo >=25){
            try{
                ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, robbery.GetGangRobberyRobXpath(),2).click();
            } catch (TimeoutException | ElementClickInterceptedException e) {
//                System.out.println("Už to odkliknul ten kokot");
            }
            try{
                ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, robbery.GetGangRobberyExecuteXpath(),2).click();
            } catch (TimeoutException | ElementNotInteractableException e) {
//                System.out.println("Už to odkliknul ten kokot");
            }
            Energyo = Integer.parseInt(statsvalue.EnergyValue());
    }
}}
