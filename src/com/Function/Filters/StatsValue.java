package com.Function.Filters;

import ObjectModel.Nightlife;
import ObjectModel.Stats;
import com.Function.MainDriver;
import org.openqa.selenium.By;

public class StatsValue extends MainDriver {
    Stats stats = new Stats();
    Nightlife nightlife = new Nightlife();


    public String EnergyValue() {
        String energy = ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, stats.GetEnergyXpath(),10).getText();
        energy = energy.replaceAll("\\D+", "");
        return energy;
    }

    public String AddictionValue() {
        String addicted = ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, stats.GetAddictionXpath(), 10).getText();
        addicted = addicted.replaceAll("\\D+", "");
        return addicted;
    }

    public String RobberyPowerValue() {
        String robberyPower = fluentWaitForElementLoad.FluentWaitForAll(driver, 500, 10, stats.GetSingleRobberyPowerXpath()).getText();
        robberyPower = robberyPower.replaceAll("\\D+", "");
        return robberyPower;
    }
    public String MyRespectValue() {
        String myRespectValue = fluentWaitForElementLoad.FluentWaitForAll(driver,1000,20,stats.GetMyRespectRobXpath()).getText();
        myRespectValue = myRespectValue.replaceAll("\\D+", "");
        return myRespectValue;
    }
    public String TargetRespectValue() {
        String targetRespectValue = fluentWaitForElementLoad.FluentWaitForAll(driver,10,2,nightlife.GetTargetRespectXpath()).getText();
//                driver.findElement(By.xpath(nightlife.GetTargetRespectXpath())).getText();
//        String targetRespectValue = WaitForElementLoad.WaitForAll(driver,nightlife.GetTargetRespectXpath() ,20).getText();
        targetRespectValue = targetRespectValue.replaceAll("\\D+", "");
        return targetRespectValue;
    }
}
