package com.Function;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class MainDriver //Hlavní kostra naší session.
{
    public static WebDriver driver;

    public static void CreateInstance() throws NoSuchElementException, InterruptedException {
      /*  System.setProperty("webdriver.gecko.driver","A:\\Geckodriver\\geckodriver.exe") ;
        driver = new FirefoxDriver();
        driver.get("https://www.thecrims.com");*/
//        System.setProperty("webdriver.chrome.driver","A:\\chromedriver\\chromedriver.exe") ;
//        driver = new ChromeDriver();
//        driver.get("https://www.thecrims.com");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\zubalikk\\Desktop\\chromedriver.exe");
//        System.setProperty("webdriver.chrome.driver","A:\\chromedriver\\chromedriver.exe") ;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
//        driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
//        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        driver.get("https://www.thecrims.com");
    }

    public static class ExplicitWaitForElementLoad {
        public static WebElement ExplicitWaitForAll(WebDriver driver, String webelement, int Seconds) {
            WebElement Webele = ExplicitwaitForElementToBeVisible(driver, webelement, Seconds);
            WebElement WebElo = ExplicitwaitForElementToClickable(driver, webelement, Seconds);
            return WebElo;
        }

        public static WebElement ExplicitwaitForElementToBeVisible(WebDriver driver, String webElement, int seconds) {

            WebDriverWait wait = (new WebDriverWait(driver, seconds));

            WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(webElement)));
            return element;
        }

        public static WebElement ExplicitwaitForElementToClickable(WebDriver driver, String webElement, int seconds) {

            WebDriverWait wait = new WebDriverWait(driver, seconds);

            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElement)));

            return element;
        }
    }

    public static class fluentWaitForElementLoad {
        public static WebElement FluentWaitForAll(WebDriver driver, int MiliSeconds, int WaitTimeout, String webElement) {
            FluentWaitForElementToBeVisible(driver, MiliSeconds, WaitTimeout, webElement);
            FluentWaitForElementToClickable(driver, MiliSeconds, WaitTimeout, webElement);
            WebElement webelo;
            webelo = driver.findElement(By.xpath(webElement));
            return webelo;
//            WebElement Webelo;
//            Webelo = FluentWaitForElementToBeVisible(driver, MiliSeconds, WaitTimeout, webElement);
//            return FluentWaitForElementToClickable(driver, MiliSeconds, WaitTimeout, webElement);
//            return Webelo;
        }

        public static WebElement FluentWaitForElementToBeVisible(WebDriver driver, int MiliSeconds, int WaitTimeout, String webElement) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(WaitTimeout, SECONDS)
                    .pollingEvery(MiliSeconds, MILLISECONDS)
                    .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
            WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(webElement)));
            return element;
        }

        public static WebElement FluentWaitForElementToClickable(WebDriver driver, int MiliSeconds, int WaitTimeout, String webElement) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(WaitTimeout, SECONDS)
                    .pollingEvery(MiliSeconds, MILLISECONDS)
                    .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElement)));
            return element;
        }
    }
}