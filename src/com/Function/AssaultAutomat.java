package com.Function;

import ObjectModel.Hospital;
import ObjectModel.Nightlife;
import ObjectModel.Robbery;
import ObjectModel.Stats;
import com.Function.Filters.StatsValue;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class AssaultAutomat extends MainDriver {
    private static class ClassMultiplier {
        private int HitmanMultiplier = 70; //zde píši procentuální hodnotu, ostatní jsou násobitelé.
        private int GangsterMultiplier = 1;
        private int RobberMultiplier = 1;
        private int DealerMultiplier = 1;
        private int BusinessmanMultiplier = 1;
        private int PimpMultiplier = 1;
        private int BrokerMultiplier= 1;
    }
    private static void AttackFirstTarget(Nightlife nightlife){
        fluentWaitForElementLoad.FluentWaitForAll(driver,10,20,nightlife.GetAssaultSelectorXpath()).click();
        fluentWaitForElementLoad.FluentWaitForAll(driver,10,20,nightlife.GetAssaultTargetInSelectXpath()).click();
        fluentWaitForElementLoad.FluentWaitForAll(driver,10,20,nightlife.GetSingleAssaultXpath()).click();
        System.out.println("Target attacked!");
        ExitClubNow(nightlife);
//        ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,nightlife.GetAssaultSelectorXpath(),20).click();
//        ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,nightlife.GetAssaultTargetInSelectXpath(),20).click();
//        ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, nightlife.GetSingleAssaultXpath(),20).click();
//        System.out.println("Target attacked!");
//        ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,nightlife.GetExitXpath(),20).click();
        try{
            System.out.println(fluentWaitForElementLoad.FluentWaitForAll(driver,300,5,"//*[@class=\"flash__message-content\"]").getText());
        } catch (TimeoutException e) {
            System.out.println("Hláška nejspíš nevyskočila");
        }
        System.out.println("____________________________________________________________________________________________________________________________________________________________________________________________________________");
    }
    private static String LowRespectLimit = "((targetRespect > respectControl) || (targetRespect < 1000))";
    private static void StatsAfterFight(Stats stats){
        int Strenght2 = Integer.parseInt(ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,stats.GetStrengthXpath(),10).getText());
        int Tolerance2 = Integer.parseInt(ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,stats.GetToleranceXpath(),10).getText());
        int Intelligence2 = Integer.parseInt(ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,stats.GetIntelligenceXpath(),10).getText());
        int Charisma2 = Integer.parseInt(ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,stats.GetCharismaXpath(),10).getText());
    }
    private static void ExitClubNow(Nightlife nightlife){
        driver.findElement(By.xpath(nightlife.GetExitXpath())).click();
//        fluentWaitForElementLoad.FluentWaitForAll(driver,10,20,nightlife.GetExitXpath()).click();
        System.out.println("Ten zmrd má hodně respektu");
    }
    public static void AssaultHunter() throws InterruptedException {
        StatsValue statsValue= new StatsValue();
        Hospital hospital = new Hospital();
        Scanner sc = new Scanner(System.in);
        Nightlife nightlife = new Nightlife();
        Stats stats = new Stats();
        Robbery robbery = new Robbery();
        ClassMultiplier classMultiplier = new ClassMultiplier();

        int energyAssault = Integer.parseInt(statsValue.EnergyValue());
        System.out.println("Vyberte v jakém klubu chcete lovit.");
        System.out.println("1 ve vlastním klubu");
        System.out.println("2 v náhodném klubu");
        String clubRemote = sc.nextLine();
        switch (clubRemote){
            case "1":
                System.out.println("Sputil jsi metodu na lovení lidí ve vlastním klubu");
                System.out.println("Zadej počet cyklů lovení, nezapomeň, že jeden cyklus je 15 sekund");
                int cycle= sc.nextInt();
                ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, nightlife.GetMenuNightlifeXpath(),10).click();
                while (!(cycle==0)){
                    energyAssault = Integer.parseInt(statsValue.EnergyValue());
                    ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, nightlife.GetMyClubXpath(),20).click();
                    if (energyAssault<50){
                        ExplicitWaitForElementLoad.ExplicitWaitForAll(driver, nightlife.GetBuyXpath(),10).click();
                    }
                    else {
                        try{
                            AttackFirstTarget(nightlife);
                        } catch (TimeoutException e) {
                            System.out.println("Lov byl neúspěšný");
                        }
                        cycle = cycle -1;
                    }
                }break;
            case "2":
                System.out.println("Sputil jsi metodu na lovení lidí v klubu");
                System.out.println("Zadej počet cyklů lovení, nezapomeň, že jeden cyklus je 15 sekund");
                int cycletwo= sc.nextInt();
                int refreshTimer = 15;
                fluentWaitForElementLoad.FluentWaitForAll(driver,500,11,nightlife.GetMenuNightlifeXpath()).click();
                while (!(cycletwo==0)){
                    try {
                        energyAssault = Integer.parseInt(statsValue.EnergyValue());
                        int myRespect = Integer.parseInt(statsValue.MyRespectValue());
                        if (energyAssault < 50) {
                            RenewEnergy.Renew();
                        } else {
                            try {
                                fluentWaitForElementLoad.FluentWaitForAll(driver,2000,10, nightlife.GetRandomClubXpath()).click();
//                                String classOfTarget = ExplicitWaitForElementLoad.ExplicitWaitForAll(driver,nightlife.GetTargetClassXpath(),15).getText();
                                String classOfTarget = fluentWaitForElementLoad.FluentWaitForAll(driver,10,12,nightlife.GetTargetClassXpath()).getText();
                                int targetRespect = Integer.parseInt(statsValue.TargetRespectValue());
                                System.out.println("Target Class="+ classOfTarget);
                                System.out.println("respekt kreténa=" + targetRespect);

                                if ((classOfTarget.contains("Hitman")) || (classOfTarget.contains("Vazoun"))){
                                    int respectControl = myRespect / 100 * classMultiplier.HitmanMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                                if ((classOfTarget.contains("Gangster"))){
                                    int respectControl = myRespect *classMultiplier.GangsterMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                                if ((classOfTarget.contains("Businessman")) || (classOfTarget.contains("Obchodník"))){
                                    int respectControl = myRespect *classMultiplier.BusinessmanMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000) || (targetRespect >= 58000 && targetRespect <= 68000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                                if ((classOfTarget.contains("Pimp")) || (classOfTarget.contains("Pasák"))){
                                    int respectControl = myRespect * classMultiplier.PimpMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                                if ((classOfTarget.contains("Robber")) || (classOfTarget.contains("Zloděj"))){
                                    int respectControl = myRespect *classMultiplier.RobberMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                                if (classOfTarget.contains("Dealer")){
                                    int respectControl = myRespect * classMultiplier.DealerMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                                if ((classOfTarget.contains("Broker")) || (classOfTarget.contains("Makléř"))){
                                    int respectControl = myRespect * classMultiplier.BrokerMultiplier;
                                    if ((targetRespect > respectControl) || (targetRespect < 1000)) {
                                        ExitClubNow(nightlife);
                                    }
                                    else {
                                        AttackFirstTarget(nightlife);
                                        refreshTimer=refreshTimer+5;
                                    }
                                }
                            }catch (TimeoutException | StaleElementReferenceException | NumberFormatException e) {
                                System.out.println("Catch pod filtrem class");
                                refreshTimer= refreshTimer-1;
                                try{
                                    ExitClubNow(nightlife);
                                }
                                catch (org.openqa.selenium.NoSuchElementException el){
                                    System.out.println("Už jsi venku :)");
                                }
                                if (refreshTimer==0){
                                    refreshTimer=refreshTimer+10;
                                    try{
                                        System.out.println("Try v If u restartu stranky");
                                        fluentWaitForElementLoad.FluentWaitForAll(driver,500,20,robbery.GetMenuRobberyXpath()).click();
                                        fluentWaitForElementLoad.FluentWaitForAll(driver,500,20,nightlife.GetMenuNightlifeXpath()).click();
                                    } catch (TimeoutException ex) {
                                        System.out.println("Catch v If u restartu stranky");
                                        fluentWaitForElementLoad.FluentWaitForAll(driver,500,20,"//*[@id=\"user-profile-avatar\"]").click();
                                        fluentWaitForElementLoad.FluentWaitForAll(driver,500,20,"//*[@class=\"pull-left\"]/img").click();
                                        System.out.println("Jo nejspíš jsi v base nebo v nemocnici.");
                                    }
                                }
                            }
                            cycletwo = cycletwo - 1;
                            System.out.println("Toto se bude ještě opakovat="+cycletwo + "/Refresh timer je aktuálně na hodnotě:"+refreshTimer);

                        }
                        energyAssault = Integer.parseInt(statsValue.EnergyValue());}
                    catch (TimeoutException | ElementClickInterceptedException ex){
                        System.out.println("Catchnulo to první TRY");
                        try{
                            System.out.println("Ted je to tady na TRY nad breakem");
                            fluentWaitForElementLoad.FluentWaitForAll(driver,500,200,"//*[@id=\"user-profile-avatar\"]").click();
                            fluentWaitForElementLoad.FluentWaitForAll(driver,500,200,"//*[@class=\"pull-left\"]/img").click();
                            fluentWaitForElementLoad.FluentWaitForAll(driver,500,20,nightlife.GetMenuNightlifeXpath()).click();
                        }
                        catch (NoSuchElementException | TimeoutException e1){
                            System.out.println("Poslední catch ve třídě");
                            System.out.println("Nejsi v nemocnici ale v base");

                        }
                    }
                }
                break;
            default:
                System.out.println("Zadal jsi nesprávnou hodnotu.");
                System.out.println("1 ve vlastním klubu");
                System.out.println("2 v náhodném klubu");

        }
    }
}